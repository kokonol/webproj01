package top.justright.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by kokonol on 2017/3/29.
 */
@Controller
public class DemoController {

    @ResponseBody
    @GetMapping("/test01")
    public String demo01(){
        return "haha";
    }

    @GetMapping("/test02")
    public String demo02(){
        return "demo/test";
    }
}

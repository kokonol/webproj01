package top.justright.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by kokonol on 2017/3/29.
 */
@SpringBootApplication
public class DemoApp {
    public static void main(String[] args){
        SpringApplication.run(DemoApp.class, args);
    }
}

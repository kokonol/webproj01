package imgtest;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by kokonol on 2017/3/30.
 */
public class TestImage {
    public static void main(String[] args) throws IOException {
        test1();
        System.out.println("Test");
    }


    public static void test1() throws IOException {
        File file = new File("/Volumes/DEV/1.png");
        File file2 = new File("/Volumes/DEV/test.png");
        BufferedImage bi = ImageIO.read(file);
        Graphics2D g2 = (Graphics2D)bi.getGraphics();

        printString(g2, "测试", 55,70, 55);
        printLine(g2,100, 3);
        printString(g2, "TEST", 90,160, 55);

        g2.drawImage(ImageIO.read(new File("/Volumes/DEV/1.jpg")), null, 10, 220);

        try {
            ImageIO.write(bi, "png", file2);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    private static void printString(Graphics2D g2 , String s, int x, int y, int size) {
        g2.setPaint(Color.BLACK);
        Font font = new Font("sansserif", Font.BOLD, size);
        g2.setFont(font);
        g2.drawString(s, x, y);
    }

    private static void printLine(Graphics2D g2, int y, int size){
        g2.setPaint(Color.BLACK);
        for(int i=0; i<size; i++){
            g2.drawLine(0,y+i,200,y+i);
        }
    }
}
